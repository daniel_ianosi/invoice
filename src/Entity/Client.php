<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client extends BaseEntity
{

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $clientType;


    /**
     * @var Receipt[]
     *
     * @ORM\OneToMany(targetEntity="Receipt", mappedBy="client")
     */
    private $receipts;


    /**
     * @var Invoice[]
     *
     * @ORM\OneToMany(targetEntity="Invoice", mappedBy="client")
     */
    private $invoices;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $uniqueRegistrationCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bank;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ibanCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commercialRegisterNumber;


    public function __construct()
    {
        $this->receipts = new ArrayCollection();
        $this->invoices = new ArrayCollection();
    }

    /**
     * @return mixed
     */


    /**
     * @return mixed
     */
    public function getClientType()
    {
        return $this->clientType;
    }

    /**
     * @param mixed $clientType
     * @return Client
     */
    public function setClientType($clientType)
    {
        $this->clientType = $clientType;
        return $this;
    }

    /**
     * @return Receipt[]
     */
    public function getReceipts(): array
    {
        return $this->receipts;
    }

    /**
     * @param Receipt[] $receipts
     * @return Client
     */
    public function setReceipts(array $receipts): Client
    {
        $this->receipts = $receipts;
        return $this;
    }





    /**
     * @return Invoice[]
     */
    public function getInvoices(): array
    {
        return $this->invoices;
    }

    /**
     * @param Invoice[] $invoices
     * @return Client
     */
    public function setInvoices(array $invoices): Client
    {
        $this->invoices = $invoices;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param mixed $county
     * @return Client
     */
    public function setCounty($county)
    {
        $this->county = $county;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Client
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param mixed $adress
     * @return Client
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Client
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUniqueRegistrationCode()
    {
        return $this->uniqueRegistrationCode;
    }

    /**
     * @param mixed $uniqueRegistrationCode
     * @return Client
     */
    public function setUniqueRegistrationCode($uniqueRegistrationCode)
    {
        $this->uniqueRegistrationCode = $uniqueRegistrationCode;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     * @return Client
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIbanCode()
    {
        return $this->ibanCode;
    }

    /**
     * @param mixed $ibanCode
     * @return Client
     */
    public function setIbanCode($ibanCode)
    {
        $this->ibanCode = $ibanCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommercialRegisterNumber()
    {
        return $this->commercialRegisterNumber;
    }

    /**
     * @param mixed $commercialRegisterNumber
     * @return Client
     */
    public function setCommercialRegisterNumber($commercialRegisterNumber)
    {
        $this->commercialRegisterNumber = $commercialRegisterNumber;
        return $this;
    }

    public function addReceipt(Receipt $receipt): self
    {
        if (!$this->receipts->contains($receipt)) {
            $this->receipts[] = $receipt;
            $receipt->setClient($this);
        }

        return $this;
    }

    public function removeReceipt(Receipt $receipt): self
    {
        if ($this->receipts->contains($receipt)) {
            $this->receipts->removeElement($receipt);
            // set the owning side to null (unless already changed)
            if ($receipt->getClient() === $this) {
                $receipt->setClient(null);
            }
        }

        return $this;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setClient($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->contains($invoice)) {
            $this->invoices->removeElement($invoice);
            // set the owning side to null (unless already changed)
            if ($invoice->getClient() === $this) {
                $invoice->setClient(null);
            }
        }

        return $this;
    }

	function __toString() {
		return $this->name;
	}


}



