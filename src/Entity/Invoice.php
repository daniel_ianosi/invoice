<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceRepository")
 */
class Invoice extends BaseEntity
{

    /**
    * @var Receipt
    *
    * @ORM\OneToOne(targetEntity="Receipt", inversedBy="invoice")
     * @ORM\JoinColumn(name="receipt_id", referencedColumnName="id")
    */
    private $receipt;

    /**
     * @var Client
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="invoices")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @var InvoiceItem[]
     * @ORM\OneToMany(targetEntity="InvoiceItem", mappedBy="invoice")
     */
    private $invoiceItems;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $invoiceType;



    /**
     * @var SeriesItem[]
     *
     * @ORM\OneToMany(targetEntity="SeriesItem", mappedBy="invoice")
     */
    private $seriesItems;



    /**
     * @var Firm
     * @ORM\ManyToOne(targetEntity="Firm", inversedBy="invoices")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id")
     */
    private $firm;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firmName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $clientName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firmAdress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $clientAdress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $clientBank;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firmBank;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firmIban;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $clientIban;

    public function __construct()
    {
        $this->invoiceItems = new ArrayCollection();
        $this->seriesItems = new ArrayCollection();
    }

	/**
	 * @return Receipt
	 */
	public function getReceipt() {
		return $this->receipt;
	}

	/**
	 * @param Receipt $receipt
	 *
	 * @return mixed
	 */
	public function setReceipt( Receipt $receipt ) {
		$this->receipt = $receipt;

		return $this;
	}

	/**
	 * @return Client
	 */
	public function getClient() {
		return $this->client;
	}

	/**
	 * @param Client $client
	 *
	 * @return Invoice
	 */
	public function setClient( Client $client ): Invoice {
		$this->client = $client;

		return $this;
	}

	/**
	 * @return InvoiceItem[]
	 */
	public function getInvoiceItems(): array {
		return $this->invoiceItems;
	}

	/**
	 * @param InvoiceItem[] $invoiceItems
	 *
	 * @return Invoice
	 */
	public function setInvoiceItems( array $invoiceItems ) {
		$this->invoiceItems = $invoiceItems;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getInvoiceType() {
		return $this->invoiceType;
	}

	/**
	 * @param mixed $invoiceType
	 *
	 * @return Invoice
	 */
	public function setInvoiceType( $invoiceType ) {
		$this->invoiceType = $invoiceType;

		return $this;
	}

	/**
	 * @return SeriesItem[]
	 */
	public function getSeriesItems(): array {
		return $this->seriesItems;
	}

	/**
	 * @param SeriesItem[] $seriesItems
	 *
	 * @return Invoice
	 */
	public function setSeriesItems( array $seriesItems ): Invoice {
		$this->seriesItems = $seriesItems;

		return $this;
	}

	/**
	 * @return Firm
	 */
	public function getFirm() {
		return $this->firm;
	}

	/**
	 * @param Firm $firm
	 *
	 * @return Invoice
	 */
	public function setFirm( Firm $firm ): Invoice {
		$this->firm = $firm;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFirmName() {
		return $this->firmName;
	}

	/**
	 * @param mixed $firmName
	 *
	 * @return Invoice
	 */
	public function setFirmName( $firmName ) {
		$this->firmName = $firmName;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getClientName() {
		return $this->clientName;
	}

	/**
	 * @param mixed $clientName
	 *
	 * @return Invoice
	 */
	public function setClientName( $clientName ) {
		$this->clientName = $clientName;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFirmAdress() {
		return $this->firmAdress;
	}

	/**
	 * @param mixed $firmAdress
	 *
	 * @return Invoice
	 */
	public function setFirmAdress( $firmAdress ) {
		$this->firmAdress = $firmAdress;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getClientAdress() {
		return $this->clientAdress;
	}

	/**
	 * @param mixed $clientAdress
	 *
	 * @return Invoice
	 */
	public function setClientAdress( $clientAdress ) {
		$this->clientAdress = $clientAdress;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getClientBank() {
		return $this->clientBank;
	}

	/**
	 * @param mixed $clientBank
	 *
	 * @return Invoice
	 */
	public function setClientBank( $clientBank ) {
		$this->clientBank = $clientBank;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFirmBank() {
		return $this->firmBank;
	}

	/**
	 * @param mixed $firmBank
	 *
	 * @return Invoice
	 */
	public function setFirmBank( $firmBank ) {
		$this->firmBank = $firmBank;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFirmIban() {
		return $this->firmIban;
	}

	/**
	 * @param mixed $firmIban
	 *
	 * @return Invoice
	 */
	public function setFirmIban( $firmIban ) {
		$this->firmIban = $firmIban;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getClientIban() {
		return $this->clientIban;
	}

	/**
	 * @param mixed $clientIban
	 *
	 * @return Invoice
	 */
	public function setClientIban( $clientIban ) {
		$this->clientIban = $clientIban;

		return $this;
	}

	function __toString() {
		return $this->firmName;
	}


}
