<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SeriesRepository")
 */
class Series extends BaseEntity
{

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;


    /**
     * @var SeriesItem[]
     *
     * @ORM\OneToMany(targetEntity="SeriesItem", mappedBy="series", cascade={"persist"}, orphanRemoval=true)
     */
    private $seriesItems;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $background;

    /**
     * @var Firm
     *
     * @ORM\ManyToOne(targetEntity="Firm", inversedBy="series")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id")
     */
    private $firm;

    public function __construct()
    {
        $this->seriesItems = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Series
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return SeriesItem[]
     */
    public function getSeriesItems(): array
    {
        return $this->seriesItems;
    }

    /**
     * @param SeriesItem[] $seriesItems
     * @return Series
     */
    public function setSeriesItems(array $seriesItems): Series
    {
        $this->seriesItems = $seriesItems;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @param mixed $background
     * @return Series
     */
    public function setBackground($background)
    {
        $this->background = $background;
        return $this;
    }

    /**
     * @return Firm
     */
    public function getFirm()
    {
        return $this->firm;
    }

    /**
     * @param Firm $firm
     * @return Series
     */
    public function setFirm(Firm $firm): Series
    {
        $this->firm = $firm;
        return $this;
    }

    public function addSeriesItem(SeriesItem $seriesItem): self
    {
        if (!$this->seriesItems->contains($seriesItem)) {
            $this->seriesItems[] = $seriesItem;
            $seriesItem->setSeries($this);
        }

        return $this;
    }

    public function removeSeriesItem(SeriesItem $seriesItem): self
    {
        if ($this->seriesItems->contains($seriesItem)) {
            $this->seriesItems->removeElement($seriesItem);
            // set the owning side to null (unless already changed)
            if ($seriesItem->getSeries() === $this) {
                $seriesItem->setSeries(null);
            }
        }

        return $this;
    }

	function __toString() {
		return $this->name;
	}


}
