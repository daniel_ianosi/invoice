<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReceiptRepository")
 */
class Receipt extends BaseEntity
{

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="receipts")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @var Series[]
     *
     * @ORM\ManyToOne(targetEntity="Series")
     * @ORM\JoinColumn(name="series_item_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $seriesItem;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @var Firm
     *
     * @ORM\ManyToOne(targetEntity="Firm", inversedBy="receipts")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id")
     */
    private $firm;



    /**
     * @var Invoice
     *
     * @ORM\OneToOne(targetEntity="Invoice", mappedBy="receipt")
     *
     */
    private $invoice;


    public function __construct()
    {
        $this->seriesItem = new ArrayCollection();
    }

	/**
	 * @return Client
	 */
	public function getClient() {
		return $this->client;
	}

	/**
	 * @param Client $client
	 *
	 * @return Receipt
	 */
	public function setClient( Client $client ): Receipt {
		$this->client = $client;

		return $this;
	}

	/**
	 * @return Series[]
	 */
	public function getSeriesItem() {
		return $this->seriesItem;
	}

	/**
	 * @param Series[] $seriesItem
	 *
	 * @return Receipt
	 */
	public function setSeriesItem(  $seriesItem ) {
		$this->seriesItem = $seriesItem;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param mixed $description
	 *
	 * @return Receipt
	 */
	public function setDescription( $description ) {
		$this->description = $description;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * @param mixed $value
	 *
	 * @return Receipt
	 */
	public function setValue( $value ) {
		$this->value = $value;

		return $this;
	}

	/**
	 * @return Firm
	 */
	public function getFirm(){
		return $this->firm;
	}

	/**
	 * @param Firm $firm
	 *
	 * @return Receipt
	 */
	public function setFirm( Firm $firm ): Receipt {
		$this->firm = $firm;

		return $this;
	}

	/**
	 * @return Invoice
	 */
	public function getInvoice() {
		return $this->invoice;
	}

	/**
	 * @param Invoice $invoice
	 *
	 * @return Receipt
	 */
	public function setInvoice( Invoice $invoice ): Receipt {
		$this->invoice = $invoice;

		return $this;
	}

	function __toString() {
		return $this->value;
	}


}
