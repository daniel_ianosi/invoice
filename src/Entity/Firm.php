<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FirmRepository")
 */
class Firm extends BaseEntity
{

    /**
     * @var Invoice[]
     *
     * @ORM\OneToMany(targetEntity="Invoice", mappedBy="firm")
     */
    private $invoices;

    /**
     * @var Receipt[]
     *
     * @ORM\OneToMany(targetEntity="Receipt", mappedBy="firm")
     */
    private $receipts;


    /**
     * @var Series[]
     *
     * @ORM\OneToMany(targetEntity="Series", mappedBy="firm")
     */
    private $series;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name='';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $uniqueRegistrationCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bank;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ibanCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commercialRegisterNumber;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paysVat;

    /**
     * @return mixed
     */
    public function getPaysVat()
    {
        return $this->paysVat;
    }

    /**
     * @param mixed $paysVat
     * @return Firm
     */
    public function setPaysVat($paysVat)
    {
        $this->paysVat = $paysVat;
        return $this;
    }




    public function __construct()
    {
        $this->invoices = new ArrayCollection();
        $this->receipts = new ArrayCollection();
        $this->series = new ArrayCollection();
    }


    /**
     * @return Invoice[]
     */
    public function getInvoices(): array
    {
        return $this->invoices;
    }

    /**
     * @param Invoice[] $invoices
     * @return Firm
     */
    public function setInvoices(array $invoices): Firm
    {
        $this->invoices = $invoices;
        return $this;
    }

    /**
     * @return Receipt[]
     */
    public function getReceipts(): array
    {
        return $this->receipts;
    }

    /**
     * @param Receipt[] $receipts
     * @return Firm
     */
    public function setReceipts(array $receipts): Firm
    {
        $this->receipts = $receipts;
        return $this;
    }

    /**
     * @return Series[]
     */
    public function getSeries(): array
    {
        return $this->series;
    }

    /**
     * @param Series[] $series
     * @return Firm
     */
    public function setSeries(array $series): Firm
    {
        $this->series = $series;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Firm
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param mixed $county
     * @return Firm
     */
    public function setCounty($county)
    {
        $this->county = $county;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Firm
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param mixed $adress
     * @return Firm
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Firm
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Firm
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUniqueRegistrationCode()
    {
        return $this->uniqueRegistrationCode;
    }

    /**
     * @param mixed $uniqueRegistrationCode
     * @return Firm
     */
    public function setUniqueRegistrationCode($uniqueRegistrationCode)
    {
        $this->uniqueRegistrationCode = $uniqueRegistrationCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     * @return Firm
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIbanCode()
    {
        return $this->ibanCode;
    }

    /**
     * @param mixed $ibanCode
     * @return Firm
     */
    public function setIbanCode($ibanCode)
    {
        $this->ibanCode = $ibanCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommercialRegisterNumber()
    {
        return $this->commercialRegisterNumber;
    }

    /**
     * @param mixed $commercialRegisterNumber
     * @return Firm
     */
    public function setCommercialRegisterNumber($commercialRegisterNumber)
    {
        $this->commercialRegisterNumber = $commercialRegisterNumber;
        return $this;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setFirm($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->contains($invoice)) {
            $this->invoices->removeElement($invoice);
            // set the owning side to null (unless already changed)
            if ($invoice->getFirm() === $this) {
                $invoice->setFirm(null);
            }
        }

        return $this;
    }

    public function addReceipt(Receipt $receipt): self
    {
        if (!$this->receipts->contains($receipt)) {
            $this->receipts[] = $receipt;
            $receipt->setFirm($this);
        }

        return $this;
    }

    public function removeReceipt(Receipt $receipt): self
    {
        if ($this->receipts->contains($receipt)) {
            $this->receipts->removeElement($receipt);
            // set the owning side to null (unless already changed)
            if ($receipt->getFirm() === $this) {
                $receipt->setFirm(null);
            }
        }

        return $this;
    }

    public function addSeries(Series $series): self
    {
        if (!$this->series->contains($series)) {
            $this->series[] = $series;
            $series->setFirm($this);
        }

        return $this;
    }

    public function removeSeries(Series $series): self
    {
        if ($this->series->contains($series)) {
            $this->series->removeElement($series);
            // set the owning side to null (unless already changed)
            if ($series->getFirm() === $this) {
                $series->setFirm(null);
            }
        }

        return $this;
    }

	function __toString() {
		return $this->name;
	}

}
