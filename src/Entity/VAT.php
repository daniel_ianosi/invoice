<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VATRepository")
 */
class VAT extends BaseEntity
{

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $value;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;


    /**
     * @var InvoiceItem[]
     * @ORM\OneToMany(targetEntity="App\Entity\InvoiceItem", mappedBy="vat")
     */
    private $invoiceItems;


    public function __construct()
    {
        $this->invoiceItems = new ArrayCollection();
    }


    public function addInvoiceItem(InvoiceItem $invoiceItem): self
    {
        if (!$this->invoiceItems->contains($invoiceItem)) {
            $this->invoiceItems[] = $invoiceItem;
            $invoiceItem->setVat($this);
        }

        return $this;
    }

    public function removeInvoiceItem(InvoiceItem $invoiceItem): self
    {
        if ($this->invoiceItems->contains($invoiceItem)) {
            $this->invoiceItems->removeElement($invoiceItem);
            // set the owning side to null (unless already changed)
            if ($invoiceItem->getVat() === $this) {
                $invoiceItem->setVat(null);
            }
        }

        return $this;
    }

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName( $name ) {
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * @param mixed $value
	 */
	public function setValue( $value ) {
		$this->value = $value;
	}

	/**
	 * @return mixed
	 */
	public function getisActive() {
		return $this->isActive;
	}

	/**
	 * @param mixed $isActive
	 */
	public function setIsActive( $isActive ) {
		$this->isActive = $isActive;
	}

	/**
	 * @return InvoiceItem[]
	 */
	public function getInvoiceItems(): array {
		return $this->invoiceItems;
	}

	/**
	 * @param InvoiceItem[] $invoiceItems
	 */
	public function setInvoiceItems( array $invoiceItems ) {
		$this->invoiceItems = $invoiceItems;
	}

}
