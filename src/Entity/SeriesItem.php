<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SeriesItemRepository")
 */
class SeriesItem extends BaseEntity
{

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $number;


    /**
     * @ORM\Column(name="date", type="date")
     */
    private $date;


    /**
     * @var Series
     *
     * @ORM\ManyToOne(targetEntity="Series", inversedBy="seriesItems")
     * @ORM\JoinColumn(name="series_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $series;


    /**
     * @var Invoice
     *
     * @ORM\ManyToOne(targetEntity="Invoice", inversedBy="seriesItems")
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     */
    private $invoice;


    /**
     * @var Receipt
     *
     * @ORM\ManyToOne(targetEntity="Receipt", inversedBy="seriesItems")
     * @ORM\JoinColumn(name="receipt_id", referencedColumnName="id")
     */
    private $receipt;


    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     * @return SeriesItem
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

	/**
	 * @return mixed
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @param mixed $date
	 *
	 * @return SeriesItem
	 */
	public function setDate( $date ) {
		$this->date = $date;

		return $this;
	}


    /**
     * @return Series
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * @param Series $series
     * @return SeriesItem
     */
    public function setSeries(Series $series): SeriesItem
    {
        $this->series = $series;
        return $this;
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param Invoice $invoice
     * @return SeriesItem
     */
    public function setInvoice(Invoice $invoice): SeriesItem
    {
        $this->invoice = $invoice;
        return $this;
    }

    /**
     * @return Receipt
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * @param Receipt $receipt
     * @return SeriesItem
     */
    public function setReceipt(Receipt $receipt): SeriesItem
    {
        $this->receipt = $receipt;
        return $this;
    }

}
