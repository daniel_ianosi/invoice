<?php

namespace App\Controller;

use App\Entity\VAT;
use App\Form\VATType;
use App\Repository\VATRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/v/a/t")
 */
class VATController extends AbstractController
{
    /**
     * @Route("/", name="v_a_t_index", methods={"GET"})
     */
    public function index(VATRepository $vATRepository): Response
    {
        return $this->render('vat/index.html.twig', [
            'v_a_ts' => $vATRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="v_a_t_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $vAT = new VAT();
        $form = $this->createForm(VATType::class, $vAT);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($vAT);
            $entityManager->flush();

            return $this->redirectToRoute('v_a_t_index');
        }

        return $this->render('vat/new.html.twig', [
            'v_a_t' => $vAT,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="v_a_t_show", methods={"GET"})
     */
    public function show(VAT $vAT): Response
    {
        return $this->render('vat/show.html.twig', [
            'v_a_t' => $vAT,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="v_a_t_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, VAT $vAT): Response
    {
        $form = $this->createForm(VATType::class, $vAT);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('v_a_t_index');
        }

        return $this->render('vat/edit.html.twig', [
            'v_a_t' => $vAT,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="v_a_t_delete", methods={"DELETE"})
     */
    public function delete(Request $request, VAT $vAT): Response
    {
        if ($this->isCsrfTokenValid('delete'.$vAT->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($vAT);
            $entityManager->flush();
        }

        return $this->redirectToRoute('v_a_t_index');
    }
}
