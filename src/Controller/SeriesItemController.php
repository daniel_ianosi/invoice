<?php

namespace App\Controller;

use App\Entity\SeriesItem;
use App\Form\SeriesItem1Type;
use App\Repository\SeriesItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/seriess/item")
 */
class SeriesItemController extends AbstractController
{
    /**
     * @Route("/", name="series_item_index", methods={"GET"})
     */
    public function index(SeriesItemRepository $seriesItemRepository): Response
    {
        return $this->render('series_item/index.html.twig', [
            'series_items' => $seriesItemRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="series_item_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $seriesItem = new SeriesItem();
        $form = $this->createForm(SeriesItem1Type::class, $seriesItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($seriesItem);
            $entityManager->flush();

            return $this->redirectToRoute('series_item_index');
        }

        return $this->render('series_item/new.html.twig', [
            'series_item' => $seriesItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="series_item_show", methods={"GET"})
     */
    public function show(SeriesItem $seriesItem): Response
    {
        return $this->render('series_item/show.html.twig', [
            'series_item' => $seriesItem,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="series_item_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SeriesItem $seriesItem): Response
    {
        $form = $this->createForm(SeriesItem1Type::class, $seriesItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('series_item_index');
        }

        return $this->render('series_item/edit.html.twig', [
            'series_item' => $seriesItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="series_item_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SeriesItem $seriesItem): Response
    {
        if ($this->isCsrfTokenValid('delete'.$seriesItem->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($seriesItem);
            $entityManager->flush();
        }

        return $this->redirectToRoute('series_item_index');
    }
}
