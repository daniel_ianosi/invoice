<?php

namespace App\Form;

use App\Entity\Invoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('createdAt')
//            ->add('updatedAt')
            ->add('owner')
            ->add('invoiceType')
            ->add('firmName')
            ->add('clientName')
            ->add('firmAdress')
            ->add('clientAdress')
            ->add('clientBank')
            ->add('firmBank')
            ->add('firmIban')
            ->add('clientIban')
            ->add('receipt')
            ->add('client')
            ->add('firm')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
        ]);
    }
}
