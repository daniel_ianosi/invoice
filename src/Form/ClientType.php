<?php

namespace App\Form;

use App\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
    //        ->add('createdAt')
    //       ->add('updatedAt')
            ->add('owner')
            ->add('clientType')
            ->add('name')
            ->add('county')
            ->add('city')
            ->add('adress')
            ->add('phone')
            ->add('email')
            ->add('uniqueRegistrationCode')
            ->add('bank')
            ->add('ibanCode')
            ->add('commercialRegisterNumber')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }
}
