<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $admin = json_encode(["Admin" => "ROLE_ADMIN"]);
        $user = json_encode(["User" => "ROLE_USER"]);
        $builder
    //        ->add('createdAt')
    //        ->add('updatedAt')
            ->add('owner')
            ->add('username')
            ->add('roles', ChoiceType::class, [
               'choices' => [
                   'Admin' => json_decode($admin,true),
                   'User' => json_decode($user,true),
               ],
                'multiple' => true,
                'expanded' => true,

            ])
            ->add('password', PasswordType::class, [
                'mapped' => true,
            ])
            ->add('name')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
