<?php

namespace App\Form;

use App\Entity\Firm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FirmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//          ->add('createdAt')
//          ->add('updatedAt')
            ->add('owner')
            ->add('name')
            ->add('county')
            ->add('city')
            ->add('adress')
            ->add('phone')
            ->add('email')
            ->add('uniqueRegistrationCode')
            ->add('bank')
            ->add('ibanCode')
            ->add('commercialRegisterNumber')
            ->add('paysVat')
//	        ->add('save', SubmitButton::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Firm::class,
        ]);
    }
}
