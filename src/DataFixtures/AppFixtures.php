<?php

namespace App\DataFixtures;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use DateTime;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

   public function __construct(UserPasswordEncoderInterface $passwordEncoder)
     {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
         $user = new User();
         $user->setName('admin');
        $user->setPassword($this->passwordEncoder->encodePassword(
                        $user,
                         'admin'));
         $user->setRoles(array('roles' =>'ROLE_ADMIN'));
         $user->setUsername('admin');
         $user->setCreatedAt(new DateTime('now'));
         $user->setUpdatedAt(new DateTime('now'));
         $user->setOwner( $user->getUsername());
         $manager->persist($user);

         //ad more users
        $manager->flush();
    }
}
